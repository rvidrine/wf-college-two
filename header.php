<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WF College Two
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'wf-college-two' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<?php get_sidebar( 'header' );
		if ( get_header_image() && ('blank' == get_header_textcolor()) ) { //Have header image but title text hidden ?>
			<figure class="header-image">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="< ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
				</a>
			</figure>
		<?php } // End header image check. 
			if ( get_header_image() && !('blank' == get_header_textcolor()) ) { //Have header image title text not hidden
				echo '<div class="site-branding header-background-image" style="background-image: url(' . get_header_image() . '); background-size: 960px 200px;">'; 
			} else {
				echo '<div class="site-branding">';
			}
		?>
			<div class="title-box">
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
	</div>
</div>

				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle">
					<i class="fa fa-bars"></i>
					</button>
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				<div id="search-container" class="search-box-wrapper clear">
					<div class="search-box clear">
						<?php get_search_form(); ?>
					</div>
				</div>
					<div class="search-toggle">
						<i class="fa fa-search"></i>
						<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'wf-college-two' ); ?></a>
					</div>
					<?php wf_college_two_social_menu(); ?>
				</nav><!-- #site-navigation -->
				
		<?php if ( function_exists( 'breadcrumb_trail' ) ) {?>
				<div id="breadcrumb-container">
<?php breadcrumb_trail( array( 
	'separator' => '&rarr;',
	'front_page' => false ) ); ?>	
	</div>
<?php } ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
