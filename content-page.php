<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WF College Two
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wf-college-two' ),
				'after'  => '</div>',
			) );
		?>
		<!-- display list of sub-pages, if the post content is empty -->
						<?php
						  $children = wp_list_pages( "title_li=&child_of=".$post->ID."&echo=0&depth=1" );
						  if ( empty( $post->post_content ) ) {
						 ?>
						  <p class="menu-placeholder-page"> It appears you've landed on a menu placeholder page. For those on a touchscreen device, here are the child pages of this page: </p>
						  <ul>
						  <?php the_title('<h3>', ' Sub-pages</h3>'); ?> 
						  <?php echo $children; ?>
						  </ul>
						  <?php }
						  else
						?>
<!-- end of show children bit (from codex.wordpress.org) -->

	</div><!-- .entry-content -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
